#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <dlfcn.h>
#include <string.h>
#include <malloc.h>

extern void *__libc_calloc(size_t nmemb, size_t size);
static void* (*real_calloc)(size_t nmemb, size_t size) = NULL;
static void* (*real_realloc)(void *ptr, size_t size) = NULL;
static void (*real_free)(void *) = NULL;
static void* (*real_malloc)(size_t) = NULL;
static int initialized = 0;
static size_t total_ = 0;

inline static void init(void) {
    static int initializing = 0;
    if(!initialized && !initializing) {
        initializing = 1;
        real_calloc = __libc_calloc;
        real_realloc = dlsym(RTLD_NEXT, "realloc");
        real_free = dlsym(RTLD_NEXT, "free");
        real_malloc = dlsym(RTLD_NEXT, "malloc");
        initializing = 0;
        initialized = 1;
    }
}

size_t heaptop_allocated() {
    return total_;
}

void *calloc(size_t nmemb, size_t size) {
    init();
    void * p = real_calloc(nmemb, size);
    total_ = __sync_add_and_fetch(&total_, malloc_usable_size(p));
    return p;
}

void *realloc(void *ptr, size_t size) {
    init();
    size_t before = malloc_usable_size(ptr);
    void * p = real_realloc(ptr, size);
    total_ = __sync_add_and_fetch(&total_, malloc_usable_size(p) - before);
    return p;
}

void free(void * p) {
    init();
    total_ = __sync_sub_and_fetch(&total_, malloc_usable_size(p));
    real_free(p);
}

void* malloc(size_t size) {
    init();
    void *p = real_malloc(size);
    total_ = __sync_add_and_fetch(&total_, malloc_usable_size(p));
    return p;
}
