#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "heaptop.h"

struct chunk_t {
    size_t size;
    void * backtrace[BACKTRACE_SIZE];
};

typedef std::map<void *, struct chunk_t> chunk_map;
chunk_map heap;

static void insert_record(Record & r) {
    struct chunk_t & c = heap[r.new_pointer];
    c.size = r.size;
    memcpy(&c.backtrace, &r.backtrace, sizeof(void*) * BACKTRACE_SIZE);
}

static int64_t play(FILE * f, int64_t end) {
    int64_t current_time = 0;
    Record r;
    while(true) {
        if(fread(&r, sizeof(r), 1, f) != 1)
            break;
        if(r.time > end)
            break;
        else
            current_time = r.time;
        switch(r.type) {
        case CALLOC:
        case MALLOC:
            insert_record(r);
            break;
        case FREE:
            heap.erase(r.old_pointer);
            break;
        case REALLOC:
            if(r.old_pointer == r.new_pointer)
                heap[r.new_pointer].size = r.size;
            else
                insert_record(r);
            break;
        }

        if(current_time > end)
            break;
    }
    return current_time;
}

static size_t allocatedSize() {
    size_t total = 0;
    for (chunk_map::reverse_iterator it = heap.rbegin(); it != heap.rend(); ++it) {
        total += it->second.size;
    }
}

static void dumpHeap() {
    for (chunk_map::reverse_iterator it = heap.rbegin(); it != heap.rend(); ++it) {
        printf("%p %ld ", it->first, it->second.size);
        for(int i = 0; i < BACKTRACE_SIZE; i++)
            printf("%p ", it->second.backtrace[i]);
        printf("\n");
        it++;
    }
}

const char * TYPES_STR[] = {
    "MALLOC", "FREE", "CALLOC", "REALLOC"
};

static void dumpEvents(FILE *f) {
    Record r;
    while(true) {
        if(fread(&r, sizeof(r), 1, f) != 1)
            break;
        printf("%ld %s %ld %p %p\n", r.time,
               TYPES_STR[r.type], r.size, r.old_pointer, r.new_pointer);
    }
}

int main(int argc, char * argv[]) {
    FILE * f = fopen("heaptop.bin", "r");
    if(std::string(argv[1]) == std::string("dump")) {
        dumpEvents(f);
    } else {
        double end = atof(argv[1]) * 1E9;
        double current_time = play(f, end);
        printf("Time: %.12g\n", current_time / 1E9);
        printf("Allocated heap size: %ld\n", allocatedSize());
        printf("Number of chunk: %d\n", heap.size());
        dumpHeap();
    }
}
