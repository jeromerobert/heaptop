#include <stddef.h>
#include <time.h>

#define BACKTRACE_SIZE 3
enum AllocType {
    MALLOC, FREE, CALLOC, REALLOC
};

struct Record {
    char type;
    void * old_pointer;
    void * new_pointer;
    size_t size;
    int64_t time;
    void * backtrace[BACKTRACE_SIZE];
};
