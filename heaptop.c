#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <string.h>
#include "heaptop.h"

extern void *__libc_calloc(size_t nmemb, size_t size);
static void* (*real_calloc)(size_t nmemb, size_t size) = NULL;
static void* (*real_realloc)(void *ptr, size_t size) = NULL;
static void (*real_free)(void *) = NULL;
static void* (*real_malloc)(size_t) = NULL;
static FILE* output;
static int initialized = 0;
static struct timespec start_time;

inline static int64_t time_diff_in_nanos(struct timespec tick, struct timespec tock) {
  return (tock.tv_sec - tick.tv_sec) * 1000000000 + (tock.tv_nsec - tick.tv_nsec);
}

inline static void init(void) {
    static int initializing = 0;
    if(!initialized && !initializing) {
        initializing = 1;
        // calloc is used by dlsym
        // real_calloc = dlsym(RTLD_NEXT, "calloc");
        real_calloc = __libc_calloc;
        real_realloc = dlsym(RTLD_NEXT, "realloc");
        real_free = dlsym(RTLD_NEXT, "free");
        real_malloc = dlsym(RTLD_NEXT, "malloc");
        output = fopen("heaptop.bin", "wb");
        if(output == NULL) {
            perror("Cannot open heaptop.bin");
        }
        backtrace(NULL, 0);
        initializing = 0;
        initialized = 1;
        clock_gettime(CLOCK_MONOTONIC, &start_time);
     }
}

inline static void write(char type, void* old_pointer, void* new_pointer, size_t size) {
    if(initialized) {
        struct Record r;
        struct timespec time;
        memset(r.backtrace, 0, BACKTRACE_SIZE);
        backtrace(r.backtrace, BACKTRACE_SIZE);
        clock_gettime(CLOCK_MONOTONIC, &time);
        r.time = time.tv_nsec;
        r.type = type;
        r.old_pointer = old_pointer;
        r.new_pointer = new_pointer;
        r.size = size;
        fwrite(&r, sizeof(r), 1, output);
    }
}

void *calloc(size_t nmemb, size_t size) {
    init();
    void * p = real_calloc(nmemb, size);
    write(CALLOC, NULL, p, size * nmemb);
    return p;
}

void *realloc(void *ptr, size_t size) {
    init();
    void * p = real_realloc(ptr, size);
    write(REALLOC, ptr, p, size);
    return p;
}

void free(void * p) {
    init();
    real_free(p);
    write(FREE, p, NULL, 0);
}

void* malloc(size_t size) {
    init();
    void *p = real_malloc(size);
    write(MALLOC, NULL, p, size);
    return p;
}
